// jetpack.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "SBDL.h"
#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;
struct Grav
{
	Texture d[2], u[2], up, down;
	bool b, b_ud, lose, lose_m;
	int counter1, counter2, counter_ud, v;
}grav;

struct Zapper
{
	Texture img[12];
	SDL_Rect rect[18];
	int x[6], y[6], choose1[6], choose2[6], counter;
	bool make1, make2;
}zapper;

struct Lazer {
	Texture img[3];
	bool b;
	SDL_Rect rect[8];
	int num;
	int begin;
	int counter;
	int limit;
	bool laz;
}lazer;

struct Gravity
{
	int x;
	int y;
}grav_item;

struct Speed_item
{
	int x;
	int y;
	int counter;
	int limit;
	int orbit1;
	int orbit2;
	bool b;
	bool show;
}speed_item;

void load();
void draw();
void update();
void Pause();
void New_start();
Texture back1, back2, back3, back4, back5, back6, back7, back8, back9;
Texture barry_running1, barry_running2, barry_rise, barry_fall, barry_boost;
Texture alien_running1, alien_running2, alien_rise, alien_fall;
Texture Home, Play, Setting, Score, Quit, Sound_key, Change_barry, pause_img, Resume, Restart, Main_menu, lose_back, soundOff;
Texture warning1, warning2, missile[7];
Texture Speed_prize[4], grav_img, score_tex, lose_tex, sec_chance;
Music *music_back;
Sound *jet_sound;
long long int x_back1, x_back2, x_back3, x_back4, x_back5, x_back6, x_back7, x_back8, x_back9;
int v_back, v_barry, a_barry, y_barry, y_warning, x_missile;
const int x_barry = 128;
SDL_Rect barry_Rect, play_Rect, setting_Rect, quit_Rect, score_Rect, sound_Rect, change_Rect, speed_Rect, grav_Rect, pause_Rect, chance_Rect, retry_Rect, menu_Rect;
SDL_Rect warning_Rect, missile_Rect;
int counter_running, counter_running2, counter_setting, counter_sound, counter_barry, counter_missile, counter_missile1, counter_missile2, score, counter_score, counter_death;
bool barry_running, barry_rising, barry_falling, home_screen, pause_screen, b_sound, Alien, b_missile, b_warning, lose_screen, bSound;
Font *font, *font_j;

int main()
{
	const int windowWidth = 1024;
	const int windowHeight = 460;
	const int delay = 30;
	SBDL::InitEngine("jetpack", windowWidth, windowHeight);
	load();
//	if (b_sound)
		SBDL::playMusic(music_back, -1);

	home_screen = true;
	New_start();

	while (SBDL::isRunning())
	{
		int startTime = SBDL::getTime();
		SBDL::updateEvents();
		SBDL::clearRenderScreen();
		draw();
		update();
		SBDL::updateRenderScreen();
	
		if (!lose_screen)
		{
			x_back1 -= v_back;
			x_back2 -= v_back;
			x_back3 -= v_back;
			x_back4 -= v_back;
			x_back5 -= v_back;
			x_back6 -= v_back;
			x_back7 -= v_back;
			x_back8 -= v_back;
			x_back9 -= v_back;
			zapper.x[0] -= v_back;
			zapper.x[1] -= v_back;
			zapper.x[2] -= v_back;
			zapper.x[3] -= v_back;
			zapper.x[4] -= v_back;
			zapper.x[5] -= v_back;
			speed_item.x -= v_back;
			grav_item.x -= v_back;
			if (x_back1 <= -2048)
				x_back1 = 16183 - 200;
			else if (x_back2 <= -2048)
				x_back2 = 16176 - 200;
			else if (x_back3 <= -2048)
				x_back3 = 16176 - 200;
			else if (x_back4 <= -1840)
				x_back4 = 16176 - 200;
			else if (x_back5 <= -2048)
				x_back5 = 16176 - 200;
			else if (x_back6 <= -2048)
				x_back6 = 16176 - 200;
			else if (x_back7 <= -2048)
				x_back7 = 16176 - 200;
			else if (x_back8 <= -2048)
				x_back8 = 16176 - 200;
			else if (x_back9 <= -2048)
				x_back9 = 16176 - 200;

			if (zapper.x[0] <= -1024)
			{
				zapper.make1 = true;
				zapper.x[0] += 2048;
				zapper.x[1] += 2048;
				zapper.x[2] += 2048;
			}
			if (zapper.x[3] <= -1024)
			{
				zapper.make2 = true;
				zapper.x[3] += 2048;
				zapper.x[4] += 2048;
				zapper.x[5] += 2048;
			}

			if (rand() % 200 == 2)
				b_missile = true;
			if (rand() % 2000 == 1 && !lazer.b)
			{
				lazer.b = true;
				lazer.num = (rand() % 5) + 1;
				lazer.begin = rand() % 8;
			}
			if (SBDL::keyPressed(SDL_SCANCODE_P) || pause_screen)
				Pause();
			counter_score++;
			if (counter_score == 7)
			{
				score++;
				counter_score = 0;
			}
		}
		SBDL::delay(30);

	}
    return 0;
}

void load()
{
	srand(time(NULL));
	back1 = SBDL::loadTexture("assets/pic/back/Lab Start.png");
	back2 = SBDL::loadTexture("assets/pic/back/Lab1.png");
	back3 = SBDL::loadTexture("assets/pic/back/Lab2.png");
	music_back = SBDL::loadMusic("assets/Sound/music.mp3");
	barry_running1 = SBDL::loadTexture("assets/pic/barry/barry.png");
	barry_running2 = SBDL::loadTexture("assets/pic/barry/barry2.png");
	barry_rise = SBDL::loadTexture("assets/pic/barry/barry3.png");
	barry_fall = SBDL::loadTexture("assets/pic/barry/barry4.png");
	barry_boost = SBDL::loadTexture("assets/pic/barry/barryst.png");
	back6 = SBDL::loadTexture("assets/pic/back/Sector Start.png");
	back5 = SBDL::loadTexture("assets/pic/back/Sector1.png");
	back4 = SBDL::loadTexture("assets/pic/back/Sector2.png");
	back7 = SBDL::loadTexture("assets/pic/back/Volcano Start.png");
	back8 = SBDL::loadTexture("assets/pic/back/Volcano1.png");
	back9 = SBDL::loadTexture("assets/pic/back/Volcano2.png");
	jet_sound = SBDL::loadSound("assets/sound/jetpack_se.wav");
	Home = SBDL::loadTexture("assets/pic/menu/Background.png");
	Play = SBDL::loadTexture("assets/pic/menu/play.png");
	Setting = SBDL::loadTexture("assets/pic/menu/setting.png");
	Score = SBDL::loadTexture("assets/pic/menu/highscores.png");
	Quit = SBDL::loadTexture("assets/pic/menu/quit.png");
	Sound_key = SBDL::loadTexture("assets/pic/menu/sound.png");
	soundOff = SBDL::loadTexture("assets/pic/menu/sound off.png");
	Change_barry = SBDL::loadTexture("assets/pic/menu/change keys.png");
	alien_running1 = SBDL::loadTexture("assets/pic/barry/alien.png");
	alien_running2 = SBDL::loadTexture("assets/pic/barry/alien2.png");
	alien_rise = SBDL::loadTexture("assets/pic/barry/alienup.png");
	alien_fall = SBDL::loadTexture("assets/pic/barry/aliendown.png");
	Speed_prize[0] = SBDL::loadTexture("assets/pic/speedToken/speed token.png");
	Speed_prize[1] = SBDL::loadTexture("assets/pic/speedToken/speed token2.png");
	Speed_prize[2] = SBDL::loadTexture("assets/pic/speedToken/speed token3.png");
	Speed_prize[3] = SBDL::loadTexture("assets/pic/speedToken/speed token4.png");
	pause_img = SBDL::loadTexture("assets/pic/menu/pauseb.png");
	grav_img = SBDL::loadTexture("assets/pic/menu/gravity_token.png");
	warning1 = SBDL::loadTexture("assets/pic/missle/1m.png");
	warning2 = SBDL::loadTexture("assets/pic/missle/2m.png");
	missile[1] = SBDL::loadTexture("assets/pic/missle/3m.png");
	missile[2] = SBDL::loadTexture("assets/pic/missle/missle (1).png");
	missile[3] = SBDL::loadTexture("assets/pic/missle/missle (2).png");
	missile[4] = SBDL::loadTexture("assets/pic/missle/missle (3).png");
	missile[5] = SBDL::loadTexture("assets/pic/missle/missle (4).png");
	missile[6] = SBDL::loadTexture("assets/pic/missle/missle (5).png");
	missile[0] = SBDL::loadTexture("assets/pic/missle/missle (6).png");
	lazer.img[0] = SBDL::loadTexture("assets/pic/lazer/laser_active1.png");
	lazer.img[1] = SBDL::loadTexture("assets/pic/lazer/laser_active2.png");
	lazer.img[2] = SBDL::loadTexture("assets/pic/lazer/laser_noneactive.png");
	zapper.img[0] = SBDL::loadTexture("assets/pic/zappers/d1.png");
	zapper.img[3] = SBDL::loadTexture("assets/pic/zappers/d2.png");
	zapper.img[6] = SBDL::loadTexture("assets/pic/zappers/d3.png");
	zapper.img[9] = SBDL::loadTexture("assets/pic/zappers/d4.png");
	zapper.img[1] = SBDL::loadTexture("assets/pic/zappers/h1.png");
	zapper.img[4] = SBDL::loadTexture("assets/pic/zappers/h2.png");
	zapper.img[7] = SBDL::loadTexture("assets/pic/zappers/h3.png");
	zapper.img[10] = SBDL::loadTexture("assets/pic/zappers/h4.png");
	zapper.img[2] = SBDL::loadTexture("assets/pic/zappers/v1.png");
	zapper.img[5] = SBDL::loadTexture("assets/pic/zappers/v2.png");
	zapper.img[8] = SBDL::loadTexture("assets/pic/zappers/v3.png");
	zapper.img[11] = SBDL::loadTexture("assets/pic/zappers/v4.png");
	Resume = SBDL::loadTexture("assets/pic/menu/resume.png");
	Restart = SBDL::loadTexture("assets/pic/menu/retry.png");
	Main_menu = SBDL::loadTexture("assets/pic/menu/back.png");
	font = SBDL::loadFont("assets/font/Jetpackia.ttf", 18);
	font_j = SBDL::loadFont("assets/font/Jetpackia.ttf", 50);
	grav.d[0] = SBDL::loadTexture("assets/pic/barry/gg1.png");
	grav.d[1] = SBDL::loadTexture("assets/pic/barry/gg2.png");
	grav.u[0] = SBDL::loadTexture("assets/pic/barry/gg3.png");
	grav.u[1] = SBDL::loadTexture("assets/pic/barry/gg4.png");
	grav.down = SBDL::loadTexture("assets/pic/barry/ggdown.png");
	grav.up = SBDL::loadTexture("assets/pic/barry/ggup.png");
	sec_chance = SBDL::loadTexture("assets/pic/menu/resume 500.png");

	chance_Rect = { 460, 260, 111, 105};
	play_Rect = { 820, 0, 200, 70 };
	setting_Rect = { 820, 80, 200, 70 };
	sound_Rect = { 820, 155, 200, 70 };
	change_Rect = { 820, 230, 200, 70 };
	score_Rect = { 820, 310, 200, 70 };
	quit_Rect = {820, 390, 200, 70};
	pause_Rect = {20, 0, 800, 466};
	menu_Rect = {280, 285,139, 50};
	retry_Rect = {605, 285, 139 ,50};
}

void draw()
{
	if (home_screen)
	{
		SBDL::showTexture(Home, 20, 0);
		SBDL::showTexture(Play, play_Rect);
		SBDL::showTexture(Setting, setting_Rect);
		SBDL::showTexture(Score, score_Rect);
		SBDL::showTexture(Quit, quit_Rect);
		if (SBDL::mouseInRect(play_Rect) && SBDL::Mouse.clicked())
			home_screen = false;
		if (SBDL::mouseInRect(setting_Rect) && SBDL::Mouse.clicked())
			counter_setting++;
		if ((counter_setting % 2) == 1)
		{
			SBDL::showTexture(Sound_key, sound_Rect);
			SBDL::showTexture(Change_barry, change_Rect);
			if (SBDL::mouseInRect(change_Rect) && SBDL::Mouse.clicked())
				counter_barry++;
			if ((counter_barry % 2) == 1)
				Alien = true;
			if (SBDL::mouseInRect(sound_Rect) && SBDL::Mouse.clicked())
				counter_sound++;
			if ((counter_sound % 2) == 1)
			{
				SBDL::stopMusic();
				SBDL::showTexture(soundOff, sound_Rect);
				bSound = true;
			}
/*			else if (b_sound)
			{
				bSound = false;
				SBDL::playMusic(music_back, -1);
			}
*/		}

			if (SBDL::mouseInRect(quit_Rect) && SBDL::Mouse.clicked())
				exit(0);

	}
	else if (lose_screen)
	{
//		SBDL::showTexture(pause_img, pause_Rect);
		SBDL::freeTexture(score_tex);
		score_tex = SBDL::createFontTexture(font, "SCORE: " + to_string(score), 250, 0, 0);
		SBDL::showTexture(score_tex, 470, 210);
		SBDL::freeTexture(lose_tex);
		lose_tex = SBDL::createFontTexture(font_j, "GAME OVER! " , 250, 0, 0);
		SBDL::showTexture(lose_tex, 400, 150);
		SBDL::showTexture(sec_chance, chance_Rect);
		SBDL::showTexture(Restart, retry_Rect);
		SBDL::showTexture(Main_menu, menu_Rect);
		if (SBDL::mouseInRect(retry_Rect) && SBDL::Mouse.clicked())
		{
			New_start();
			lose_screen = false;
		}
		if (SBDL::mouseInRect(menu_Rect) && SBDL::Mouse.clicked())
		{
			New_start();
			lose_screen = false;
			home_screen = true;
		}
	}
	else if (pause_screen)
	{
		SBDL::showTexture(pause_img, pause_Rect);
		SBDL::showTexture(Setting, setting_Rect);
		SBDL::showTexture(Resume, play_Rect);
		SBDL::showTexture(Restart, score_Rect);
		SBDL::showTexture(Main_menu, quit_Rect);

		if (SBDL::mouseInRect(setting_Rect) && SBDL::Mouse.clicked())
			counter_setting++;
		if (SBDL::mouseInRect(play_Rect) && SBDL::Mouse.clicked())
			pause_screen = false;
		if (SBDL::mouseInRect(score_Rect) && SBDL::Mouse.clicked())
		{
			New_start();
			pause_screen = false;
		}
		if (SBDL::mouseInRect(quit_Rect) && SBDL::Mouse.clicked())
		{
			New_start();
			pause_screen = false;
			home_screen = true;
		}
		if (SBDL::mouseInRect(quit_Rect) && SBDL::Mouse.clicked())
		{
			pause_screen = false;
			home_screen = true;
		}
		if (SBDL::mouseInRect(sound_Rect) && SBDL::Mouse.clicked())
			counter_sound++;
		
		if ((counter_setting % 2) == 1)
		{
			SBDL::showTexture(Sound_key, sound_Rect);
			SBDL::showTexture(Change_barry, change_Rect);
			if (SBDL::mouseInRect(change_Rect) && SBDL::Mouse.clicked())
				counter_barry++;
			if ((counter_barry % 2) == 1)
				Alien = true;
			if ((counter_sound % 2) == 1)
			{
				SBDL::stopMusic();
				SBDL::showTexture(soundOff, sound_Rect);
				bSound = true;
			}
		}
	}
	else
	{
		SBDL::showTexture(back1, x_back1, 0);
		SBDL::showTexture(back2, x_back2, 0);
		SBDL::showTexture(back3, x_back3, 0);
		SBDL::showTexture(back4, x_back4, 0);
		SBDL::showTexture(back5, x_back5, 0);
		SBDL::showTexture(back6, x_back6, 0);
		SBDL::showTexture(back7, x_back7, 0);
		SBDL::showTexture(back8, x_back8, 0);
		SBDL::showTexture(back9, x_back9, 0);
		if (!grav.b)
			SBDL::showTexture(grav_img, grav_Rect);
		SBDL::freeTexture(score_tex);
		score_tex = SBDL::createFontTexture(font, "SCORE: " + to_string(score), 0, 0, 0);
		SBDL::showTexture(score_tex, 10, 10);

		if (speed_item.show)
		{
			if (speed_item.orbit1 == 0)
				SBDL::showTexture(Speed_prize[0], speed_Rect);
			else if (speed_item.orbit1 == 1)
				SBDL::showTexture(Speed_prize[1], speed_Rect);
			else if (speed_item.orbit1 == 2)
				SBDL::showTexture(Speed_prize[2], speed_Rect);
			else
				SBDL::showTexture(Speed_prize[3], speed_Rect);
			speed_item.orbit2++;
			if (speed_item.orbit2 == 4)
			{
				++speed_item.orbit1;
				speed_item.orbit1 = speed_item.orbit1 % 4;
				speed_item.orbit2 = 0;
			}
		}
		if (zapper.choose1[0] && !lazer.laz)
		{
			SBDL::showTexture(zapper.img[zapper.choose2[0] + (++zapper.counter % 4) * 3], zapper.rect[zapper.choose2[0]]);
			if (SBDL::hasIntersectionRect(barry_Rect, zapper.rect[zapper.choose2[0]]))
			{
				if (speed_item.b || grav.b)
					grav.lose = true;
				else
				{
					lose_screen = true;
					lazer.b = false;
					b_missile = false;
				}
			}
		}
		if (zapper.choose1[1] && !lazer.laz)
		{ 
			SBDL::showTexture(zapper.img[zapper.choose2[1] + (++zapper.counter % 4) * 3], zapper.rect[zapper.choose2[1] + 3]);
			if (SBDL::hasIntersectionRect(barry_Rect, zapper.rect[zapper.choose2[1] + 3]))
			{
				if (speed_item.b || grav.b)
					grav.lose = true;
				else
				{
					lose_screen = true;
					lazer.b = false;
					b_missile = false;
				}
			}
		}
		if (zapper.choose1[2] && !lazer.laz)
		{ 
			SBDL::showTexture(zapper.img[zapper.choose2[2] + (++zapper.counter % 4) * 3], zapper.rect[zapper.choose2[2] + 6]);
			if (SBDL::hasIntersectionRect(barry_Rect, zapper.rect[zapper.choose2[2] + 6]))
			{
				if (speed_item.b || grav.b)
					grav.lose = true;
				else
				{
					lose_screen = true;
					lazer.b = false;
					b_missile = false;
				}
			}
		}
		if (zapper.choose1[3] && !lazer.laz)
		{ 
			SBDL::showTexture(zapper.img[zapper.choose2[3] + (++zapper.counter % 4) * 3], zapper.rect[zapper.choose2[3] + 9]);
			if (SBDL::hasIntersectionRect(barry_Rect, zapper.rect[zapper.choose2[3] + 9]))
			{
				if (speed_item.b || grav.b)
					grav.lose = true;
				else
				{
					lose_screen = true;
					lazer.b = false;
					b_missile = false;
				}
			}
		}
		if (zapper.choose1[4] && !lazer.laz)
		{ 
			SBDL::showTexture(zapper.img[zapper.choose2[4] + (++zapper.counter % 4) * 3], zapper.rect[zapper.choose2[4] + 12]);
			if (SBDL::hasIntersectionRect(barry_Rect, zapper.rect[zapper.choose2[4] + 12]))
			{
				if (speed_item.b || grav.b)
					grav.lose = true;
				else
				{
					lose_screen = true;
					lazer.b = false;
					b_missile = false;
				}
			}
		}
		if (zapper.choose1[5] && !lazer.laz)
		{ 
			SBDL::showTexture(zapper.img[zapper.choose2[5] + (++zapper.counter % 4) * 3], zapper.rect[zapper.choose2[5] + 15]);
			if (SBDL::hasIntersectionRect(barry_Rect, zapper.rect[zapper.choose2[5] + 15]))
			{
				if (speed_item.b || grav.b)
					grav.lose = true;
				else
				{
					lose_screen = true;
					lazer.b = false;
					b_missile = false;
				}
			}
		}

		if (grav.lose && (!SBDL::hasIntersectionRect(barry_Rect, zapper.rect[zapper.choose2[5] + 15]) && !SBDL::hasIntersectionRect(barry_Rect, zapper.rect[zapper.choose2[4] + 12]) && !SBDL::hasIntersectionRect(barry_Rect, zapper.rect[zapper.choose2[3] + 9]) && !SBDL::hasIntersectionRect(barry_Rect, zapper.rect[zapper.choose2[2] + 6]) && !SBDL::hasIntersectionRect(barry_Rect, zapper.rect[zapper.choose2[1] + 3]) && !SBDL::hasIntersectionRect(barry_Rect, zapper.rect[zapper.choose2[0]])))
		{
//			grav.lose = false;
			grav.b = false;
		}
	}
	barry_Rect = { x_barry, y_barry, 45, 54};
	speed_Rect = { speed_item.x, speed_item.y, 42, 42 };
	grav_Rect = {grav_item.x, grav_item.y, 58, 58};
	warning_Rect = {x_missile, y_warning, 51, 51};
	missile_Rect = { x_missile, y_warning, 71, 39 };
	lazer.rect[0] = { 2, 0, 1024, 50 };
	lazer.rect[1] = { 2, 50, 1024, 50 };
	lazer.rect[2] = { 2, 100, 1024, 50 };
	lazer.rect[3] = { 2, 150, 1024, 50 };
	lazer.rect[4] = { 2, 200, 1024, 50 };
	lazer.rect[5] = { 2, 250, 1024, 50 };
	lazer.rect[6] = { 2, 300, 1024, 50 };
	lazer.rect[7] = { 2, 350, 1024, 50 };
	zapper.rect[0] = { zapper.x[0], zapper.y[0], 102, 97 };
	zapper.rect[3] = { zapper.x[1], zapper.y[1], 102, 97 };
	zapper.rect[6] = { zapper.x[2], zapper.y[2], 102, 97 };
	zapper.rect[9] = { zapper.x[3], zapper.y[3], 102, 97 };
	zapper.rect[12] = { zapper.x[4], zapper.y[4], 102, 97 };
	zapper.rect[15] = { zapper.x[5], zapper.y[5], 102, 97 };
	zapper.rect[1] = { zapper.x[0], zapper.y[0], 140, 51 };
	zapper.rect[4] = { zapper.x[1], zapper.y[1], 140, 51 };
	zapper.rect[7] = { zapper.x[2], zapper.y[2], 140, 51 };
	zapper.rect[10] = { zapper.x[3], zapper.y[3], 140, 51 };
	zapper.rect[13] = { zapper.x[4], zapper.y[4], 140, 51 };
	zapper.rect[16] = {zapper.x[5], zapper.y[5], 140, 51};
	zapper.rect[2] = { zapper.x[0], zapper.y[0], 51, 140 };
	zapper.rect[5] = { zapper.x[1], zapper.y[1], 51, 140 };
	zapper.rect[8] = { zapper.x[2], zapper.y[2], 51, 140 };
	zapper.rect[11] = { zapper.x[3], zapper.y[3], 51, 140 };
	zapper.rect[14] = { zapper.x[4], zapper.y[4], 51, 140 };
	zapper.rect[17] = { zapper.x[5], zapper.y[5], 51, 140 };

}

void update()
{
//	srand(time(NULL));
	if (speed_item.x <= -20)
	{
		speed_item.x += speed_item.limit;
		speed_item.y = rand() % 400;
	}
	if (!home_screen && !pause_screen)
	{
		if (SBDL::hasIntersectionRect(barry_Rect, grav_Rect))
		{
			grav.b = true;
			v_barry = 0;
		}
		if (y_barry > 380)
			y_barry = 380;
		else if (y_barry < 0)
		{
			y_barry = 0;
			v_barry = 0;
		}
		counter_running = counter_running % 2;
		if (grav.b)
		{
			if (grav.b_ud)
			{
				if (grav.counter_ud % 2 == 0)
					SBDL::showTexture(grav.up, barry_Rect);
				else
					SBDL::showTexture(grav.down, barry_Rect);
			}
			else if (grav.counter_ud % 2 == 0)
			{
				if (y_barry < 380)
				{
					y_barry += grav.v;
					SBDL::showTexture(grav.d[1], barry_Rect);
				}
				else
					SBDL::showTexture(grav.d[grav.counter2], barry_Rect);
			}
			else
			{
				if (y_barry > 0)
				{
					y_barry -= grav.v;
					SBDL::showTexture(grav.u[1], barry_Rect);
				}
				else
					SBDL::showTexture(grav.u[grav.counter2], barry_Rect);
			}
			grav.counter1++;
			if (grav.counter1 == 4)
			{
				grav.counter2++;
				grav.counter2 = grav.counter2 % 2;
				grav.counter1 = 0;
				grav.b_ud = false;
			}
			if (SBDL::keyPressed(SDL_SCANCODE_SPACE))
			{
				grav.counter_ud++;
				grav.b_ud = true;
			}
		}
		else
		{
			if (speed_item.b)
			{
				if (Alien)
					SBDL::showTexture(alien_rise, barry_Rect);
				else
					SBDL::showTexture(barry_boost, barry_Rect);
			}
			else if (SBDL::keyHeld(SDL_SCANCODE_SPACE))
			{
				if (Alien)
					SBDL::showTexture(alien_rise, barry_Rect);
				else
					SBDL::showTexture(barry_rise, barry_Rect);
//									SBDL::playSound(jet_sound, 1);
				v_barry += a_barry;
				y_barry -= v_barry;
			}
			else if (y_barry >= 375)
			{
				v_barry = 0;
				if (counter_running == 0)
				{
					if (Alien)
						SBDL::showTexture(alien_running1, barry_Rect);
					else
						SBDL::showTexture(barry_running1, barry_Rect);
				}
				else
				{
					if (Alien)
						SBDL::showTexture(alien_running2, barry_Rect);
					else
						SBDL::showTexture(barry_running2, barry_Rect);
				}
				counter_running2++;
				if (counter_running2 == 4)
				{
					counter_running++;
					counter_running2 = 0;
				}
			}
			else
			{
				if (Alien)
					SBDL::showTexture(alien_fall, barry_Rect);
				else
					SBDL::showTexture(barry_fall, barry_Rect);
				v_barry -= a_barry;
				y_barry -= v_barry;
			}
		}

		if (b_missile)
		{
			if (counter_missile < 100 && !b_warning)
			{
				counter_missile++;
				y_warning = y_barry; 
				SBDL::showTexture(warning1, warning_Rect);
			}
			else if (b_warning && counter_missile >= 0)
			{
				SBDL::showTexture(warning2, warning_Rect);
				counter_missile--;
			}
			else if (counter_missile == 100)
				b_warning = true;
			else
			{
				SBDL::showTexture(missile[counter_missile1], missile_Rect);
				counter_missile2++;
				if (counter_missile2 == 3)
				{
					counter_missile1++;
					counter_missile1 = counter_missile1 % 7;
					counter_missile2 = 0;
				}
				x_missile -= v_back*2;
			}
			if (x_missile < -100)
			{
				b_warning = false;
				b_missile = false;
				x_missile = 1024 - 56;
			}
		}

		if (lazer.b)
		{
			if (lazer.limit < 100 && !lazer.laz)
			{
				switch (lazer.num)
				{
				case (1) : SBDL::showTexture(lazer.img[2], lazer.rect[lazer.begin]);
					break;
				case (2) : SBDL::showTexture(lazer.img[2], lazer.rect[lazer.begin]);
					SBDL::showTexture(lazer.img[2], lazer.rect[(lazer.begin + 1) % 8]);
					break;
				case (3) : SBDL::showTexture(lazer.img[2], lazer.rect[lazer.begin]);
					SBDL::showTexture(lazer.img[2], lazer.rect[(lazer.begin + 1) % 8]);
					SBDL::showTexture(lazer.img[2], lazer.rect[(lazer.begin + 2) % 8]);
					break;
				case (4) : SBDL::showTexture(lazer.img[2], lazer.rect[lazer.begin]);
					SBDL::showTexture(lazer.img[2], lazer.rect[(lazer.begin + 1) % 8]);
					SBDL::showTexture(lazer.img[2], lazer.rect[(lazer.begin + 2) % 8]);
					SBDL::showTexture(lazer.img[2], lazer.rect[(lazer.begin + 3) % 8]);
					break;
				case (5) : SBDL::showTexture(lazer.img[2], lazer.rect[lazer.begin]);
					SBDL::showTexture(lazer.img[2], lazer.rect[(lazer.begin + 1) % 8]);
					SBDL::showTexture(lazer.img[2], lazer.rect[(lazer.begin + 2) % 8]);
					SBDL::showTexture(lazer.img[2], lazer.rect[(lazer.begin + 3) % 8]);
					SBDL::showTexture(lazer.img[2], lazer.rect[(lazer.begin + 4) % 8]);
					break;
				}
				lazer.limit++;
			}
			else if (lazer.laz && lazer.limit < 300)
			{
				lazer.limit++;
				switch (lazer.num)
				{
				case (1) : SBDL::showTexture(lazer.img[lazer.counter], lazer.rect[lazer.begin]);
					if (SBDL::hasIntersectionRect(barry_Rect, lazer.rect[lazer.begin]))
					{
						if (speed_item.b || grav.b)
							grav.lose = true;
						else
						{
							lose_screen = true;
							lazer.b = false;
							b_missile = false;
						}
					}
					break;
				case (2) : SBDL::showTexture(lazer.img[lazer.counter], lazer.rect[lazer.begin]);
					SBDL::showTexture(lazer.img[lazer.counter], lazer.rect[(lazer.begin + 1) % 8]);
					if (SBDL::hasIntersectionRect(barry_Rect, lazer.rect[lazer.begin]) || SBDL::hasIntersectionRect(barry_Rect, lazer.rect[(lazer.begin + 1) % 8]))
					{
						if (speed_item.b || grav.b)
							grav.lose = true;
						else
						{
							lose_screen = true;
							lazer.b = false;
							b_missile = false;
						}
					}
					break;
				case (3) : SBDL::showTexture(lazer.img[lazer.counter], lazer.rect[lazer.begin]);
					SBDL::showTexture(lazer.img[lazer.counter], lazer.rect[(lazer.begin + 1) % 8]);
					SBDL::showTexture(lazer.img[lazer.counter], lazer.rect[(lazer.begin + 2) % 8]);
					if (SBDL::hasIntersectionRect(barry_Rect, lazer.rect[lazer.begin]) || SBDL::hasIntersectionRect(barry_Rect, lazer.rect[(lazer.begin + 1) % 8]) || SBDL::hasIntersectionRect(barry_Rect, lazer.rect[(lazer.begin + 2) % 8]))
					{
						if (speed_item.b || grav.b)
							grav.lose = true;
						else
						{
							lose_screen = true;
							lazer.b = false;
							b_missile = false;
						}
					}
					break;
				case (4) : SBDL::showTexture(lazer.img[lazer.counter], lazer.rect[lazer.begin]);
					SBDL::showTexture(lazer.img[lazer.counter], lazer.rect[(lazer.begin + 1) % 8]);
					SBDL::showTexture(lazer.img[lazer.counter], lazer.rect[(lazer.begin + 2) % 8]);
					SBDL::showTexture(lazer.img[lazer.counter], lazer.rect[(lazer.begin + 3) % 8]);
					if (SBDL::hasIntersectionRect(barry_Rect, lazer.rect[lazer.begin]) || SBDL::hasIntersectionRect(barry_Rect, lazer.rect[(lazer.begin + 1) % 8]) || SBDL::hasIntersectionRect(barry_Rect, lazer.rect[(lazer.begin + 2) % 8]) || SBDL::hasIntersectionRect(barry_Rect, lazer.rect[(lazer.begin + 3) % 8]))
					{
						if (speed_item.b || grav.b)
							grav.lose = true;
						else
						{
							lose_screen = true;
							lazer.b = false;
							b_missile = false;
						}
					}
					break;
				case (5) : SBDL::showTexture(lazer.img[lazer.counter], lazer.rect[lazer.begin]);
					SBDL::showTexture(lazer.img[lazer.counter], lazer.rect[(lazer.begin + 1) % 8]);
					SBDL::showTexture(lazer.img[lazer.counter], lazer.rect[(lazer.begin + 2) % 8]);
					SBDL::showTexture(lazer.img[lazer.counter], lazer.rect[(lazer.begin + 3) % 8]);
					SBDL::showTexture(lazer.img[lazer.counter], lazer.rect[(lazer.begin + 4) % 8]);
					if (SBDL::hasIntersectionRect(barry_Rect, lazer.rect[lazer.begin]) || SBDL::hasIntersectionRect(barry_Rect, lazer.rect[(lazer.begin + 1) % 8]) || SBDL::hasIntersectionRect(barry_Rect, lazer.rect[(lazer.begin + 2) % 8]) || SBDL::hasIntersectionRect(barry_Rect, lazer.rect[(lazer.begin + 3) % 8]) || SBDL::hasIntersectionRect(barry_Rect, lazer.rect[(lazer.begin + 4) % 8]))
					{
						if (speed_item.b || grav.b)
							grav.lose = true;
						else
						{
							lose_screen = true;
							lazer.b = false;
							b_missile = false;
						}
					}
					break;
				}
				lazer.counter++;
				lazer.counter = lazer.counter % 2;
			}
			else if (lazer.limit == 100)
			{
				lazer.laz = true;
				lazer.limit = 0;
			}
			else
			{
				lazer.b = false;
				lazer.limit = 0;
				lazer.laz = false;
			}
		}

		if (SBDL::hasIntersectionRect(barry_Rect, speed_Rect))
		{
			speed_item.b = true;
			v_back = 16;
		}
		if (speed_item.b)
		{
			if (speed_item.counter < 100)
			{
				speed_item.counter++;
				speed_item.show = false;
			}
			if (speed_item.counter >= 100)
			{
				speed_item.counter = 0;
				v_back = 8;
				speed_item.show = true;
				speed_item.b = false;
			}
		}
		if (zapper.make1)
		{
			zapper.choose1[0] = rand() % 2;
			zapper.choose1[1] = rand() % 2;
			zapper.choose1[2] = rand() % 2;
			zapper.choose2[0] = rand() % 3;
			zapper.choose2[1] = rand() % 3;
			zapper.choose2[2] = rand() % 3;
			zapper.y[0] = rand() % 340;
			zapper.y[1] = rand() % 340;
			zapper.y[2] = rand() % 340;
			zapper.make1 = false;
		}
		if (zapper.make2)
		{
			zapper.choose1[3] = rand() % 2;
			zapper.choose1[4] = rand() % 2;
			zapper.choose1[5] = rand() % 2;
			zapper.choose2[3] = rand() % 3;
			zapper.choose2[4] = rand() % 3;
			zapper.choose2[5] = rand() % 3;
			zapper.y[3] = rand() % 340;
			zapper.y[4] = rand() % 340;
			zapper.y[5] = rand() % 340;
			zapper.make2 = false;
		}
		if (SBDL::hasIntersectionRect(barry_Rect, missile_Rect))
		{
			if (speed_item.b || grav.b)
				grav.lose_m = true;
			else
			{
				lose_screen = true;
				b_missile = false;
				lazer.b = false;
			}
		}
		if (grav.lose_m && !SBDL::hasIntersectionRect(barry_Rect, missile_Rect))
		{
//			grav.lose = false;
			grav.b = false;
		}
	}
}

void Pause()
{
	pause_screen = true;
}

void New_start()
{
	x_back1 = 0;
	x_back2 = 2048;
	x_back3 = 4096;
	x_back4 = 5936;
	x_back5 = 7984 - 200;
	x_back6 = 10032 - 200;
	x_back7 = 12080 - 200;
	x_back8 = 14128 - 200;
	x_back9 = 16176 - 200;
	v_back = 8;
	y_barry = 380;
	counter_running = 0;
	counter_running2 = 0;
	a_barry = 1;
	speed_item.limit = 10000;
	speed_item.x = 2000;
	speed_item.y = rand() % 400;
	grav_item.x = (rand() % 2250) + speed_item.x;
	grav_item.y = rand() % 400;
	speed_item.show = true;
	x_missile = 1024 - 56;
	zapper.x[0] = 400;
	zapper.x[1] = zapper.x[0] + 300;
	zapper.x[2] = zapper.x[1] + 300;
	zapper.x[3] = zapper.x[2] + 300;
	zapper.x[4] = zapper.x[3] + 300;
	zapper.x[5] = zapper.x[4] + 300;
	zapper.y[0] = rand() % 340;
	zapper.y[1] = rand() % 340;
	zapper.y[2] = rand() % 340;
	zapper.y[3] = rand() % 340;
	zapper.y[4] = rand() % 340;
	zapper.y[5] = rand() % 340;
	zapper.make2 = true;
	zapper.make1 = false;
	grav.v = 10;
	lazer.b = false;
	score = 0;
	b_missile = false;
	grav.lose = false;
	grav.lose_m = false;
}